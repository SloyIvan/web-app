@extends('layout')

@section('content')

         <div class="container">
            <h3>Записная книжка</h3>
            <a href="{{ route('tasks.create') }}" class="btn btn-success">Создать</a>
            <div class ="row">
                <div class="col-md-10 col-md-offset-1">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Задача</td>
                                <td>Действия</td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td>{{$task->id}}</td>
                                <td>{{$task->title}}</td>
                                <td>

                                    <a href="{{route('tasks.show', $task->id)}}" class="btn btn-primary btn-sm">
                                        <span class="glyphicon glyphicon-plus"></span> Просмотреть
                                    </a>

                                    <a href="{{ route('tasks.edit', $task->id) }}" class="btn btn-warning btn-sm">
                                        <span class="glyphicon glyphicon-edit"></span> Редактировать
                                    </a>


                                    {!! Form::open(['method' => 'DELETE',
                                    'route'=> ['tasks.destroy', $task->id]]) !!}
                                    <button onclick="return confirm('Вы уверены?')">
                                        <span class="glyphicon glyphicon-remove"></span> Удалить</button>
                                    {!! Form::close() !!}


                                </td>
                            </tr>
                         @endforeach
                        </tbody>
                     </table>
                </div>
             </div>
         </div>

@endsection
