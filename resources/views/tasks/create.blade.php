@extends('layout')



@section('content')

@include('errors')

         <div class="container">
            <h3>Создать запись</h3>

            <div class="row">
                <div class="col-md-12">

                {!! Form::open(['route' => ['tasks.store']]) !!}
                    <div class="form-group">
                        <input v-model="message" name ="title" placeholder="Заголовок">
                        <br>
                        <textarea v-model="message" name ="description"  placeholder="Описание"></textarea>
                        <br>
                        <button class="btn btn-success">Подтвердить</button></br></br><a href="{{ route('tasks.index') }}" class="btn btn-success">Отменить</a>
                    </div>
                {!! Form::close() !!} 

                 </div>
            </div>

         </div>
@endsection
